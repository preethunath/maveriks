package com.classpath.javatrack.pack1;

public class PackageB {

    private int privateVariable = 11;
    protected int protectedVariable = 22;
    int defaultVariable = 33;
    public int publicVariable = 44;

    
    public static void main(String[] args) {
        PackageA obj = new PackageA();
        //System.out.println("Private variable "+ obj.privateVariable);
        System.out.println("Protected variable "+ obj.protectedVariable);
        System.out.println("Default variable "+ obj.defaultVariable);
        System.out.println("Public variable "+ obj.publicVariable);
    }
}
