# try handling an exception 
try:

    # will result in arithmetic exception
    result = 45 / 10
    array = [33, 55,66,88]
    #will result in index out of bounds error
    array[4]

except ZeroDivisionError as e:
   # print("cannot divide a number by 0")
    print(type(e))
    print(e)

except IndexError as e:
   # print("cannot divide a number by 0")
    print(type(e))
    print(e)
else:
    print("this block will only be executed if there is no exception")
finally:
    print("The block will be executed if an exception is thrown or not. Usually done for clean up operations")