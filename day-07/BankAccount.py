class BankAccount: 
    def __init__(self, branch, amount):
        self.branch = branch
        self.amount = amount
    

    def withdraw(self, withdrawAmt):
        if(self.amount >= withdrawAmt):
            self.amount -= withdrawAmt

    def deposit(self, depositAmt):
        self.amount = self.amount + depositAmt

    def check_balance(self):
        return self.amount

class SavingsAccount(BankAccount):

    def __init__(self, branch, amount):
        super().__init__(branch, amount )

    def check_balance(self):
        print("Overriding the check balance from the BankAccount class")
        return self.amount * 0.12
# Creating the account from base class
#suresh = BankAccount('ICICI -  RR-Nagar', 2000)
#print("Before balance ", suresh.check_balance())

#suresh.deposit(45000)

#print("After deposit ", suresh.check_balance())

savingsAccount = SavingsAccount("HDFC", 45000)

print(savingsAccount.check_balance())