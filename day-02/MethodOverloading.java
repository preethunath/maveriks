public class MethodOverloading {

    public int sum(int a, long b ){
        return (int) (a + b);
    }

    public int sum(long a, int b){
        return (int)(a + b);
    }

    public void sum(String a, String b){
        System.out.println("Inside the concat method "+ (a + b));
    }

    public static void main(String[] args) {
        MethodOverloading demo = new MethodOverloading();
        demo.sum("Hello ", "World");
    }
}
