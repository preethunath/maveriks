public class SavingsAccount extends BankAccount {

    public SavingsAccount(String accountName, double initialDeposit){
        super(accountName, initialDeposit);
    }

    @Override
    public void deposit(double amountToBeDeposited){
        //custom implementatin
        if (amountToBeDeposited > 600000){
            //verifiyPan
        }
        if (amountToBeDeposited > 50000){
            System.out.println(" You cannot deposit more than 50000 Rs at one time");
        }else {
            super.deposit(amountToBeDeposited);
        }
    }

    public void printAccountStatement(){
        System.out.println(" Printing Savings Account statement.");
    }
    
}
