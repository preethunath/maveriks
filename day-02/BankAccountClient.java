
import java.util.Scanner;

public class BankAccountClient {

    public static void main(String[] args) {
       /* SalariedAccount salariedAccount = new SalariedAccount("Ramesh", 10_000);
        salariedAccount.deposit(56000);
        System.out.println(" Account balance from Salaried Account"+ salariedAccount.getBalance());

        SavingsAccount savingsAccount = new SavingsAccount("Suresh", 10_000);
        savingsAccount.deposit(25000);
        System.out.println(" Account balance from Savings Account "+ savingsAccount.getBalance());
        
        CurrentAccount currentAccount = new CurrentAccount("Rakesh");
        currentAccount.deposit(15000);
        System.out.println(" Account balance from current account "+ currentAccount.getBalance());
        */

        // Polymorphism

        // Reference(base class) handle = new ChildClass();

        /*
            String input = "1";
            if (args[0] != null){
                input = args[0];
            }
        */

        System.out.println(" Please select from the options :");
        System.out.println("1 -> SavingsAccount ");
        System.out.println("2 -> CurrentAccount ");
        System.out.println("3 -> SalariedAccount ");

        Scanner sc = new Scanner(System.in);
        int option = sc.nextInt();
        BankAccount bankAccount = null;
        switch (option) {
            case 1:
                bankAccount = new SavingsAccount("Ramesh", 10000);
                break;
            case 2:
                bankAccount = new CurrentAccount("Ramesh", 10000);
                break;
            case 3:
                bankAccount = new SalariedAccount("Ramesh", 10000);
                break;
            default:
                bankAccount = new SalariedAccount("Ramesh", 10000);
                break;
        }
        bankAccount.deposit(250000000);
        sc.close();
    }
    
}
