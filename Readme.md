# Object Oriented Programming 

## Features of OOO 
- Encapsulation
- Abstration
- Inheritance
- Polymorphism 
  
## Objects 
- First class citizens in OOO
  - Objects can be assigned to a variable 
  - Objects can be passed as an argument
  - Object can be returned from a method
- Objects are the realization of a class
- Objects are created from the Class definition 
- Modifications done to an object will be specific to that object
- Objects are stored in Heap memory 
- Modifications to an object can be done using its references
- Memory allocation is done by JVM 
- When an object does not have any references, the JVM will reclaim the memory

## Class 
- Class is a definition/template 
- Class definition contains members 
  - Members constitutes of data and behaviour - Encapsultation 
  - Data is referred with 
    - instance variables 
  - Methods are called instance methods
  - Class definition is created using the `class` keyword 
  - The access specifier for a class can be `public` or `default`
  - Default values of Instance variables 
    - int, float, long,short, byte = 0
    - boolean = false 
    - objects = null  
  - An object of a class is created using its constructor 

## Constructor
 - The constructor is called while creating an object of a class using the new keyword
 - The name of the constructor should be the same as the class name
 - The constructor does not have a return type
 - All the member initialization is done within the constructor
 - If you do not create a constructor, the compiler will create a default constructor
 - The default constructor will not accept any arguments.
 - If you explicitly create a constructor, the compiler will not create a default 
    constructor.

 ## Overloading
 - Same method name but
   - Different number of arguments
   - Different types of arguments
   - Resolved at compile time
   - Return type is not considered   

## Inheritance
- Way to reusing the functionality
- Child class used the `extends` keyword 
- A class can extend only one class 
- The parent class is also referred to base class, parent class 
- The child class is also referred to a sub class, derived class 
- The parent/child is always relative 
- Once a class extends from Parent class, it automatically inherits all the properties and methods from the parent class

## Inheritance Rules
- The reference should always be of Parent class 
- The implementation should always be of Child class
- Using the reference, we can only call the method which is present in the parent class
- In addition to the methods defined in the parent class, the child class can have additional methods which will not be known in the parent class.
- Overriding happends within the parent class and the child class
- The handle dictates which method can be called.
- The implementation class will dictate which method will be invoked 
- To override a method
  - The method name should be same.
  - The number of arguments should be same 
  - To ensure that you are overriding, prefer to use the `@Override` annotation.

## Exceptions
 - CheckedException and UncheckedException (RuntimeException)
 - Exception class extends RuntimeException are called UncheckedException
 - Exception class extends Exception are called CheckedException
 - UncheckedException are not enforced to be handled by the compiler
 - CheckedException are enforced to be handled by the compiler 
 - The method should either handle the exception using try and catch or add the `throws` clause in the method signature
 - Even UncheckedException can be part of the `throws` clause
 - If the method throws Exception, the caller should handle the exception using the try/catch or should declare the excpetion in the throws clause 
 - Create a custom Exception by either extending Excpeption class or RuntimeException
 - With regards to Overriding rule, the overriding method cannot throw any new CheckedException which is not declared in the parent class.
 - A try block should be accompanied with either a catch block or a finally block
 - You can have multiple catch blocks provided the more generic exception class follow the more specific exception cach block
 - Never have empty catch block in the code.
 - Finally block will be executed whether an exception is thrown or not
 - Finally block is executed to clean up the resources 
 - Handle multiple Exceptions using the pipe operator 

 ```
 catch(ItemNotFoundException | IOException exception){
            //System.out.println("Item with the given id is not present");
            System.out.println(exception.getMessage());
        }
 ```

## Access Specifiers 
 There are 4 access specifiers 
  - public - Least restrictive
  - protected 
  - default (package private)
  - private  - Most restrictive

  - At a class level, you can only specify public/default
  - If a class is public it is visible for everyone 
  - All instance/class members can have all access specifiers 
  - If a class or member is declared as `default`, then it is visible only within package 
  - If a member is defined as protected, it is visible within the package and outside of the package through inheritance 
  - Private members can only be accessed within the class and not even by the subclass
  - When overriding, you can only broaden the access specifier.
 


 ### Package
  
  - Package declaration should be the first line in the class 
  - `package` should the keyword 
  - The convention for package is the reverse internet domain `com.classpath.maveriks.` 
  - If a class is default, then the class along with its members are not visible outside of the package, even in case, the members are public 
  - the import statements should follow the package declaration in a java file 
  - If a member is protected, it can be accessed within the package and outside the package using the subclass 
  - All the classes/interfaces from the java.lang package are imported by default. You do not have to import them explicitly. The compiler will insert the import statements for the `java.lang` package
  - You can import all the classes/interfaces from a package by using the `.*` syntax. 
  - When importing all the classes/interfaces, the sub packages are not imported.   

  ## Java IO 
  - All Classes and Interfaces for handling IO is bundled in `java.io` package
  - IO can be divided into Streams and Character
  - For all bytes, use the Streams 
  - For all text data, use Character 
  - Input and Output 
  - Streams - InputStream and OutputStream
  - Characters - InputReader and Writer  
  

 ## Collection API 
  - Drawbacks of Array
    - fixed size
    - By default all elements will be allocated with default values
    - Difficult to remove elements from the middle 
    - No methods, there is only one length property

 - Collections were introduced in Java 
   - Works only with Objects 
   - Wrapper classes for all primitive data types 
   - Wrapper classes are part of `java.lang` package
   - Collection API has methods which can be invoked
   - Collection Interface
     - Iterable Interace extends Collection  
       - List interface
         - Index bases 
         - ArrayList backed by Array
         - LinkedList backed by Node
       - Set Interface
         - No duplicates 
         - HashSet
         - TreeSet
       - Queue Interface 
         - FIFO queus
         - PriorityQueue
  
## Set 
  - Set does not allow duplicates
  - HashSet and TreeSet are concrete implementations of Set Interface 
  - HashSet uses the hash and equals method 
  - The elements that will be in the HashSet should Override the `equals` and `hashCode` method from the Object class
  - TreeSet allows for Ordering the elements 
  - To allow elements to be placed in the TreeSet, implement the `Comparable` interface and override the `compareTo` method 
  - To iterate over the set use the `Iterator` interface 

## Map
 - Hold key and value 
 - Keys cannot be duplicate and values can be duplicate 
 - Concrete implementations are HashSet and TreeSet 
 - The keys should Override the `equals` and `hashCode` method 
 - Similarly for TreeSet, The class which holds the key should implement the `Comparable` interface
