print(" Hello world")

# single line comment
operand1 = 45;
operand2 = 56
"""
  This is a documentation comment
"""
print(operand1 + operand2)

flag = False;
print("Value of flag is ", flag)

list = [11,22,33,44,55]
list[4] = 66;

print("The item at index 1 is ", list[4])

# tuple 

fruits=("apple", "mango", "banana")

print(fruits)
print(fruits[0])

#set
# It is unordered, unchangable and does not allow duplicates
pets = {"cat", "dog", "cow", "buffallow", "parrot"}

print(len(pets))
