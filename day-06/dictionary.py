user={
    "name":"Vinay",
    "place":"Bangalore",
    "age": 38,
    "address": {
        "city": "Bangalore",
        "zip": 577142,
        "married": False,
        "hobbies":["singing", "cricket"]
    }
}

#print all the values
print(user)

#print the value based on key 
print(user["place"])

print(user["address"]["hobbies"])

#print the type of the object 
print(type(user))