import java.util.Set;
import java.util.TreeSet;

/**
 * This will be doc comment
 * @since 1.0
 * @author Pradeep
 * @version 1.0.0
 */
public class TreeSetDemo {

    /**
     * This is the main method
     * @param args
     */
	public static void main(String[] args) {
        // single line comment 

        /*
           This is a multi line comment
        */
		Set<Item> items = new TreeSet<>();
		Item item1 = new Item(12, "IPad", 35000);
		Item item2 = new Item(14, "IPad Mini", 84000);
		Item item3 = new Item(17, "IPad Pro", 12000);
		Item item4 = new Item(88, "IPad Micra", 6000);
		
		items.add(item1);
		items.add(item2);
		items.add(item3);
		items.add(item4);
		
		System.out.println(items);
	}
}
