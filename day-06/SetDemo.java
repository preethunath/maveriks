import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {
	
	public static void main(String[] args) {
		Item item1 = new Item(12, "IPad", 34000);
		Item item3 = new Item(12, "IPad", 34000);
		Item item2 = item1;
		
		Set<Item> itemSet = new HashSet<>();
		
		itemSet.add(item1);
		itemSet.add(item2);
		itemSet.add(item3);
		
		
		System.out.println("Size of set is "+ itemSet.size());
		
		Iterator<Item> it  = itemSet.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		

		//System.out.println(item2.getPrice());
		//System.out.println(item3.getPrice());
		
		System.out.println("Is item1 and item2 equal" + item1.equals(item2));
		System.out.println("Is item1 and item3 equal" + item1.equals(item3));
		
		System.out.println("Changing the attribute of item1");
		item1.setName("New IPad");
		item3.setName("New IPad");
		
		System.out.println("Is item1 and item3 equal" + item1.equals(item3));

	}

}
