public class Keywords {
    public static void main(String args[]){

        //number, boolean, float, char
        // data type variable_name = value;
        /*
          Rules to be followed while declaring variables
          1. The variable names should be alpha numeric [a-zA-Z 0-9 _ $]
          2. A variable name cannot start with a digit 
          3. Java is case sensitive java != Java
          4. Variable names cannot be a keyword (if, while, true, boolean, class)
        */

        /*
          Conventions while declaring a varialbe
          1. for a Class, the first letter should be capital
          2. For a method name, all the characters should be small
          3. If more than words, then it should follow camelCase naming convention
          4. Declare meaningfull names, do not use, i,j,k. Use index, flag
        */

        //declaration
        int declaraingVariagble;

        //assignment
        declaraingVariagble = 45;

        //variable assignment
        int intValue = 23; 

        boolean value = false;

        int [] array = new int [10];

        System.out.println("The length of an array is "+ array.length);

    }
}