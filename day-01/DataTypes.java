public class DataTypes {
    public static void main(String args[]){

        //whole numbers - Unsinged Megative and positive numbers
        byte byteValue = 33;  // 1 byte - 8 bits
        short shortValu = 455; // 2 bytes - 16 bits
        int intValue = 12;  // 4 bytes - 32 bits
        long longValue = 4566L; //  8 bytes - 64 bits
        
        //real numbers
        double doubleValue = 4555.8899; // 8 bytes
        float floatValue = 4344.3434F; // 4 bytes
 
        //boolean (true | false)
        boolean flag = false;  // 1 bytes (only 1 bit will be used)

        //characters
        char charValue = 'c'; // 2 bytes - Signed (Only positive numbers)

        System.out.println("The integer value is "+ intValue);
    }
}