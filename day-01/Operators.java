public class Operators {
    public static void main(String [] args){

        System.out.println("Argument passed : "+ args[0]);
        //Arithmetic operators
        int result = 45 + 67;
        int product = 56 * 56;
        int modulo = 56 % 4;
        int divisionResult = 55 / 5;   
        
        System.out.println("=================Artithmetic Operators starts ====================");
        System.out.printf("The result of addition is %d %n", result);
        System.out.printf("The result of product is %d %n", product);
        System.out.printf("The result of modulo is %d %n", modulo);
        System.out.printf("The result of division is %d %n", divisionResult);
        System.out.println("=================Artithmetic Operators end ====================");

        //Relational operators
        boolean greaterThan = 56 > 45;
        boolean lessThan = 45 > 600;
        boolean equalTo = 522 == 600;
        boolean notEqualTo = 522 != 600;

        System.out.println("=================Relational Operators start ====================");
        System.out.printf("Greater than result %s %n", greaterThan);
        System.out.printf("Less than result %s %n", lessThan);
        System.out.printf("Result of equal to  %s %n", equalTo);
        System.out.printf("Result of not equal to %s %n", notEqualTo);
        System.out.println("=================Relational Operators end ====================");
        
        
        System.out.println("=================Logical Operators starts ====================");
        System.out.println("Logical AND " + (true && true));
        System.out.println("Logical OR " +  (false || true));
        System.out.println("=================Logical Operators end ====================");

        

    }
}