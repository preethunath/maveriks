public class Method {
    public static void main(String[] args) {
        String name = args[0];
        greet(name);
        int result = calc(45, 56);
        System.out.printf("The result is %d %n", result);
    }

    static void greet(String message){
        System.out.println("Hello "+ message);
    }

    static int calc ( int operand1, int operand2){
        return operand1 + operand2;
    }

    //access specified access-modifiers return-type name(args)
    // private|protected| public final|synchronized|static void|data-type name(args)
}