public interface PaymentGateway {

    public static final String CURRENCY = "INR";
    
    public abstract void acceptPayment(String from, String to, double amount, String notes);
}
