public interface ItemService {

    void saveItem(Item item);

    Item[] fetchItems();

    Item findItemById(long itemId) throws ItemNotFoundException;

    void deleteItemById(long itemId);
    
}
