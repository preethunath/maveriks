public class OrthoPadecian extends Doctor {
    
    public OrthoPadecian(int exp, String name){
        super(exp, name);
        System.out.println(" Inside the constrcutor of OrthoPadecian..");
    }

    @Override
    public void treatPatient(){
        conductXRay();
        conductCTScan();
    }

    public void conductXRay(){
        System.out.println("Conducting X Ray");
    }

    public void conductCTScan(){
        System.out.println("Conducting CT Scan");
    }
}
